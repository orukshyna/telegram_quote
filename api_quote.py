import logging
import os
import random

import requests
from dotenv import load_dotenv
from telegram import ReplyKeyboardMarkup
from telegram.ext import CommandHandler, Updater

load_dotenv()

TOKEN = os.getenv('TOKEN_QUOTE')
URL = os.getenv('API_URL')
DEFAULT_QUOTE = ('One of the most difficult things is not to change society '
                 '- but to change yourself. \n  Nelson Mandela')

emojis = ['🌳', '🌺', '☕', '🙂', '☀', '⛅', '☔',
             '🔥', '☄', '🚣', '🏡', '🌅', '🌇', '🌉', '🚲',
             '⛵', '✈', '🪂', '🚀', '🌌']


def get_random_quote():
    try:
        response = requests.get(URL)
    except requests.exceptions.RequestException as request_exception:
        logging.error('Exception during request to the main API: '
                      f'{request_exception}')
        return DEFAULT_QUOTE
    random_quote = (response.json()[0]['q'] + '\n'
                    + response.json()[0]['a'] + ' '
                    + emojis[random.randint(0, 21)])
    return random_quote


def new_quote(update, context):
    chat = update.effective_chat
    context.bot.send_message(chat.id, get_random_quote())


def start_app(update, context):
    chat = update.effective_chat
    name = update.message.chat.first_name
    button = ReplyKeyboardMarkup([['/new_quote']], resize_keyboard=True)

    context.bot.send_message(
        chat_id=chat.id,
        text=f'Hello, {name}!',
        reply_markup=button
    )
    context.bot.send_message(chat.id, get_random_quote())


def main():
    updater = Updater(token=TOKEN, use_context=True)

    updater.dispatcher.add_handler(CommandHandler('start', start_app))
    updater.dispatcher.add_handler(CommandHandler('new_quote', new_quote))

    updater.start_polling(5.0)
    updater.idle()


if __name__ == '__main__':
    logging.basicConfig(
        level=logging.INFO,
        filename=__file__ + '.log',
        format=('%(asctime)s, %(levelname)s, '
                '%(message)s, %(name)s'),
        filemode='a',)

    main()

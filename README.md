# telegram_quote

### telegram_quote - is application for telegram bot which is based on:
* free zenquotes.io API;
* requests, python-dotenv and python-telegram-bot libraries.

### to use telegram bot just:
* install Telegram;
* find the bot by @olhas_quote_bot;
* initiate the program;
* request the new quote with the related button.